#!/bin/bash

# Author: Fahad Alghathbar
# Created on: April 14, 2023

# اسم المجلد الذي سيتم إنشاء الملفات المصغرة فيه
output_folder="resized"
mkdir -p "$output_folder"

# الوقت الحالي قبل بداية تنفيذ البرنامج
start_time=$(date +%s)

# التحقق مما إذا كانت أداة 'convert' مثبتة
if command -v convert &> /dev/null
then
    # قائمة الامتدادات المدعومة
    supported_extensions=("jpg" "png")

    # تكرار العمليات لكل امتداد
    for ext in "${supported_extensions[@]}"
    do
        # الحصول على جميع الملفات بامتداد معين
        files=(*."$ext")
        total_files=${#files[@]}

        # التحقق مما إذا كان هناك ملفات بهذا الامتداد قبل البدء في عملية التحجيم
        if [ "$total_files" -gt 0 ]; then
            count=0
            echo "Resizing $total_files .$ext files..."

            # تكرار عمليات التحجيم
            for file in *."$ext"
            do
                ((count++))
                echo "[$count/$total_files] Resizing $file"
                convert "$file" -resize 50% -colorspace RGB "$output_folder/$file"

            done
        fi
    done

    # الوقت بعد انتهاء تنفيذ البرنامج
    end_time=$(date +%s)
    duration=$((end_time - start_time))
    echo "Finished resizing images in $duration seconds."
else
    echo "Error: 'convert' tool not found. Please install ImageMagick."
fi
